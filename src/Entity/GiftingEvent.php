<?php

namespace App\Entity;

/**L'entité évément, qui sert à regrouper des participants et leurs listes de cadeaux */
class GiftingEvent {

    private ?int $id;

	/*Le nom de l'événement*/
    private string $eventName;
	
    public function __construct(string $eventName, ?int $id = null) {
    	$this->id = $id;
    	$this->eventName = $eventName;
    }

	/*Ci-dessous les getters et setters des différentes propriétés, servant à y accéder depuis d'autres fichiers du code malgré leur visibilité private*/

	/**
	 * @return ?int
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getEventName(): string {
		return $this->eventName;
	}
	
	/**
	 * @param string $eventName 
	 * @return self
	 */
	public function setEventName(string $eventName): self {
		$this->eventName = $eventName;
		return $this;
	}
}
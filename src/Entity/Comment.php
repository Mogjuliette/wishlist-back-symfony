<?php

namespace App\Entity ;

/*Classe commentaire, qui permet d'ajouter des commentaires en dessous de chaque cadeau */
class Comment {

    private ?int $id ;

	/**Le contenu du commentaire */
    private string $text ;

	/**L'utilisateur (user) qui poste le commentaire */
    private int $idUser ;

	/**Le cadeau (gift) auquel est rataché le commentaire */
    private int $idGift ;

	/**La visibilité du commentaire : si is public est true, le userWantingGift peut le lire, sinon non */
	private bool $isPublic;


 /**
  * Summary of __construct
  * @param string $text 
  * @param int $idUser
  * @param int $idGift
  * @param bool $isPublic
  * @param mixed $id
  */
	public function __construct(string $text, int $idUser, int $idGift,  bool $isPublic, ?int $id = null) {
    	$this->id = $id;
    	$this-> text = $text;
    	$this-> idUser = $idUser;
        $this-> idGift = $idGift;
		$this-> isPublic = $isPublic;
    }

	/*Ci-dessous les getters et setters des différentes propriétés, servant à y accéder depuis d'autres fichiers du code malgré leur visibilité private*/

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getText(): string {
		return $this->text;
	}
	
	/**
	 * @param string $text 
	 * @return self
	 */
	public function setText(string $text): self {
		$this->text = $text;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdUser(): int {
		return $this->idUser;
	}
	
	/**
	 * @param int $idUser 
	 * @return self
	 */
	public function setIdUser(int $idUser): self {
		$this->idUser = $idUser;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdGift(): int {
		return $this->idGift;
	}
	
	/**
	 * @param int $idGift 
	 * @return self
	 */
	public function setIdGift(int $idGift): self {
		$this->idGift = $idGift;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getIsPublic(): bool {
		return $this->isPublic;
	}
	
	/**
	 * @param bool $isPublic 
	 * @return self
	 */
	public function setIsPublic(bool $isPublic): self {
		$this->isPublic = $isPublic;
		return $this;
	}
}
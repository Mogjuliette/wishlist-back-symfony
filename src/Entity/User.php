<?php

namespace App\Entity;

use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/*L'entité utilisateur, qui est aussi la base de notre authentification, pour cela on rajoute le implements UserInterface, PasswordAuthenticatedUserInterface*/
class User implements UserInterface, PasswordAuthenticatedUserInterface {
    private ?int $id;
    private string $username;
	private string $email;
    private string $password;
	private ?string $role;

    public function __construct(string $username, string $email, string $password, ?string $role = null, ?int $id = null) {
    	$this->id = $id;
    	$this->username = $username;
		$this->email = $email;
    	$this->password = $password;
		$this->role = $role;
    }

	/*Ci-dessous les getters et setters des différentes propriétés, servant à y accéder depuis d'autres fichiers du code malgré leur visibilité private*/


    /**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string {
		return $this->email;
	}

	/**
	 * @param string $username 
	 * @return self
	 */
	public function setEmail(string $email): self {
		$this->email = $email;
		return $this;
	}

    /**
	 * @return string
	 */
	public function getUsername(): string {
		return $this->username;
	}
	
	/**
	 * @param string $username 
	 * @return self
	 */
	public function setUsername(string $username): self {
		$this->username = $username;
		return $this;
	}

     /**
	 * @return string
	 */
	public function getPassword(): string {
		return $this->password;
	}
	
	/**
	 * @param string $password 
	 * @return self
	 */
	public function setPassword(string $password): self {
		$this->password = $password;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getRole(): string {
		return $this->role;
	}
	
	/**
	 * @param string $role 
	 * @return self
	 */
	public function setRole(string $role): self {
		$this->role = $role;
		return $this;
	}

	/**
     * Méthode indiquant les rôles du User.
     */
    public function getRoles(): array {
        return [$this->role];
	}

	public function setRoles(string $role){
		[$this->role] = [$role];
		return $this;
	}
	/**
     * Méthode qui sert à remettre à zéro les données sensibles dans l'entité pour ne pas les persistées
     * (par exemple si on avait un champ pour le mot de passe en clair différent du champ password)
     */
	public function eraseCredentials() {
	}
	/**
     * Méthode indiquant à Symfony Security l'identifiant du user, ici le mail.
     */
	public function getUserIdentifier(): string {
        return $this->email;
	}


}
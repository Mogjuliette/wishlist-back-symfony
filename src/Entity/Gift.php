<?php

namespace App\Entity ;

/**Entité cadeau */
class Gift {

    private ?int $id;

	/**Définit l'utilisateur qui souhaite obtenir le cadeau */
    private int $idUserWantingGift;

	/**L'événement auquel se rattache ce cadeau */
	private int $idEvent;

	/**Le nom du cadeau */
    private string $name;

	/**Une description du cadeau */
    private ?string $description;

	/**L'URL de l'image du cadeau */
    private ?string $image;

	/**Si le booléen isOffered est true, quelqu'un compte offrir ce cadeau */
    private bool $isOffered;
    
	/**L'utilisateur qui veut offrir le cadeau*/
    private ?int $idUserOfferingGift;

	/**
	 * Summary of __construct
	 * @param int $idUserWantingGift
	 * @param int $idEvent
	 * @param string $name
	 * @param string $description
	 * @param string $image
	 * @param bool $isOffered
	 * @param mixed $id
	 * @param mixed $idUserOfferingGift
	 */
    public function __construct(int $idUserWantingGift, int $idEvent, string $name, ?string $description = null,  bool $isOffered, ?int $id = null, ?int $idUserOfferingGift = null, ?string $image = null,) {
    	$this->id = $id;
    	$this-> idUserWantingGift = $idUserWantingGift;
		$this-> idEvent = $idEvent;
    	$this-> name = $name;
        $this-> description = $description;
        $this-> image = $image;
        $this-> isOffered = $isOffered;
        $this -> idUserOfferingGift = $idUserOfferingGift;
    }
	
	/*Ci-dessous les getters et setters des différentes propriétés, servant à y accéder depuis d'autres fichiers du code malgré leur visibilité private*/


	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdUserWantingGift(): int {
		return $this->idUserWantingGift;
	}
	
	/**
	 * @param int $idUserWantingGift 
	 * @return self
	 */
	public function setIdUserWantingGift(int $idUserWantingGift): self {
		$this->idUserWantingGift = $idUserWantingGift;
		return $this;
	}
	
	public function getIdEvent(): int {
		return $this->idEvent;
	}
	
	/**
	 * @param int $idEvent 
	 * @return self
	 */
	public function setIdEvent(int $idEvent): self {
		$this->idEvent = $idEvent;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}
	
	/**
	 * @param string $description 
	 * @return self
	 */
	public function setDescription(string $description): self {
		$this->description = $description;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getImage(): string {
		return $this->image;
	}
	
	/**
	 * @param string $image 
	 * @return self
	 */
	public function setImage(string $image): self {
		$this->image = $image;
		return $this;
	}
	
	/**
	 * @return bool
	 */
	public function getIsOffered(): bool {
		return $this->isOffered;
	}
	
	/**
	 * @param bool $isOffered 
	 * @return self
	 */
	public function setIsOffered(bool $isOffered): self {
		$this->isOffered = $isOffered;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getIdUserOfferingGift(): ?int {
		return $this->idUserOfferingGift;
	}
	
	/**
	 * @param  $idUserOfferingGift 
	 * @return self
	 */
	public function setIdUserOfferingGift(?int $idUserOfferingGift): self {
		$this->idUserOfferingGift = $idUserOfferingGift;
		return $this;
	}

	/**
	 * @return int
	 */
	
}
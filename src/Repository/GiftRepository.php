<?php

namespace App\Repository;

use App\Entity\Gift;
use PDO;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*Dans notre repository, on code les différentes méthodes correspondant au CRUD (CREATE, READ, UPDATE, DELETE) ainsi que des méthodes customs lorsque l'on a un besoin plus spécifique*/
class GiftRepository{

    /**On instancie une connexion à la base de données avec PDO */
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * permet de trouver tous les cadeaux
     * @return array Gift
     */
    public function findall(): array
    {
        $gifts = [];


        $statement = $this->connection->prepare('SELECT * FROM gift');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $gifts[] = $this->sqlToGift($item);
        }
        return $gifts;
    
    }

    /**
     * Méthode permettant de trouver un cadeau en fonction de son id
     * @param int $id
     * @return Gift|null
     */
    public function findById(int $id): Gift|null{
        $statement = $this->connection->prepare('SELECT * FROM gift WHERE id = :id');
        $statement->bindParam(':id', $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();
        if($result === false){
            throw new NotFoundHttpException(); 
        }
        $gift = $this->sqlToGift($result);
        return $gift;
    }

    /**
     * Méthode pour factoriser la transformation d'une requête sql (tableau) en entités Gift
     * @param array
     * @return Gift
     */
    private function sqlToGift(array $item):Gift {
        return new Gift($item['id_userWantingGift'], $item['id_event'], $item['name'], $item['description'], $item['isOffered'], $item['id'], $item['id_userOfferingGift'], $item['image']);
    }

    /**
     * Pour enregistrer un nouveau cadeau dans la base de données
     * @param Gift
     * @return void
     */
    public function persist(Gift $gift): void{
        $statement = $this->connection->prepare('INSERT into gift (id_userWantingGift, id_event, name, description, image, isOffered, id_userOfferingGift) VALUES (:id_userWantingGift, :id_event, :name, :description, :image, :isOffered, :id_userOfferingGift)');
        $statement->bindValue('id_userWantingGift', $gift->getIdUserWantingGift(), PDO::PARAM_INT);
        $statement->bindValue('id_event', $gift->getIdEvent(), PDO::PARAM_INT);
        $statement->bindValue('name', $gift->getName(), PDO::PARAM_STR);
        $statement->bindValue('description', $gift->getDescription(), PDO::PARAM_STR);
        $statement->bindValue('image', $gift->getImage(), PDO::PARAM_STR);
        $statement->bindValue('isOffered', $gift->getIsOffered(), PDO::PARAM_BOOL);
        $statement->bindValue('id_userOfferingGift', $gift->getIdUserOfferingGift(), PDO::PARAM_INT);
        $statement->execute();
        $gift->setId($this->connection->lastInsertId());
    }

    /**
     * Pour mettre à jour un cadeau dans la base de données
     * @param Gift
     * @return void
     */
    public function update(Gift $gift) {
        $statement = $this->connection->prepare('UPDATE gift SET id_userWantingGift=:id_userWantingGift,id_event=:id_event,name=:name,description=:description,image=:image,isOffered=:isOffered,id_userOfferingGift=:id_userOfferingGift WHERE id=:id');
        $statement->bindValue('id_userWantingGift', $gift->getIdUserWantingGift(), PDO::PARAM_INT);
        $statement->bindValue('id_event', $gift->getIdEvent(), PDO::PARAM_INT);
        $statement->bindValue('name', $gift->getName(), PDO::PARAM_STR);
        $statement->bindValue('description', $gift->getDescription(), PDO::PARAM_STR);
        $statement->bindValue('image', $gift->getImage(), PDO::PARAM_STR);
        $statement->bindValue('isOffered', $gift->getIsOffered(), PDO::PARAM_BOOL);
        $statement->bindValue('id_userOfferingGift', $gift->getIdUserOfferingGift(), PDO::PARAM_INT);
        $statement->bindValue('id', $gift->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Pour supprimer un cadeau
     * @param Gift
     * @return void
     */
    public function delete(Gift $gift) {
        $statement = $this->connection->prepare('DELETE FROM gift WHERE id=:id');
        $statement->bindValue('id', $gift->getId(), PDO::PARAM_INT);
        

        $statement->execute();
    }

    /**
     * Pour trouver tous les cadeaux associés à un utilisateur et un événement
     * @param int
     * @return array Gift
     */
    public function findByUserAndEvent(int $idUser, int $idEvent):array{

        $array = [];

        $statement = $this->connection->prepare('SELECT gift.id, `id_userWantingGift`, `id_event`, name, description, image, `isOffered`, `id_userOfferingGift` FROM `gift` LEFT JOIN `user` ON gift.id_userWantingGift=`user`.id LEFT JOIN `giftingEvent`  ON gift.id_event =`giftingEvent`.id WHERE gift.id_userWantingGift = :id_user AND gift.id_event = :id_event ;
        ');

        $statement->bindValue('id_user', $idUser, PDO::PARAM_INT);
        $statement->bindValue('id_event', $idEvent, PDO::PARAM_INT);


        $statement->execute();

        $results = $statement->fetchAll();
        if($results){
        foreach ($results as $line) {
            $array[] = $this->sqlToGift($line);
        }
        }
        return $array;
    }
}
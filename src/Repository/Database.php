<?php

namespace App\Repository;

use PDO;

/**Ici, on  externalise la connexion à la base de donnée afin de ne pas la réécrire dans chaque fichier */
class Database
{
   public static function connect()
    {
        return new PDO("mysql:host={$_ENV['DATABASE_HOST']};dbname={$_ENV['DATABASE_NAME']}", $_ENV['DATABASE_USER'], $_ENV['DATABASE_PASSWORD']);
    }
}

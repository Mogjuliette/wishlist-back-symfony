<?php

namespace App\Repository;

use App\Entity\Comment;
use PDO;

/*Dans notre repository, on code les différentes méthodes correspondant au CRUD (CREATE, READ, UPDATE, DELETE) ainsi que des méthodes customs lorsque l'on a un besoin plus spécifique*/
class CommentRepository{

    /**On instancie une connexion à la base de données avec PDO */
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * @return array comment
     */
    public function findall(): array
    {
        $comments = [];


        $statement = $this->connection->prepare('SELECT * FROM comment');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $comments[] = $this->sqlToComment($item);
        }
        return $comments;
    
    }

    /**
     * @return Comment|null
     */
    public function findById(int $id): Comment|null{
        $statement = $this->connection->prepare('SELECT * FROM comment WHERE id = :id');
        $statement->bindParam(':id', $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();
        if($result === false){
            return null;
        }
        $user = $this->sqlToComment($result);
        return $user;
    }

    /**
     * Méthode pour factoriser la transformation d'une requête sql (tableau) en entités Comment
     * @return Comment
     */
    private function sqlToComment(array $item):Comment {
        return new Comment($item['text'], $item['id_user'], $item['id_gift'], $item['isPublic'], $item['id']);
    }

    /**
     * enregistre un nouveau commentaire dans la base de données
     * @param Comment
     * @return void
     */
    public function persist(Comment $comment): void{
        $statement = $this->connection->prepare('INSERT INTO comment (text, id_user, id_gift, isPublic) VALUES (:text, :id_user, :id_gift, :isPublic)');
        $statement->bindValue('text', $comment->getText());
        $statement->bindValue('id_user', $comment->getIdUser());
        $statement->bindValue('id_gift', $comment->getIdGift());
        $statement->bindValue('isPublic', $comment->getIsPublic());

        $statement->execute();
        $comment->setId($this->connection->lastInsertId());
    }

    /**
     * Permet de mettre à jour un commentaire
     * @param Comment
     * @return void
     */
    public function update(Comment $comment):void {
        $statement = $this->connection->prepare('UPDATE comment SET text = :text, id_user = :id_user, id_gift = :id_gift, isPublic = :isPublic WHERE id=:id');
        $statement->bindValue('text', $comment->getText(), PDO::PARAM_STR);
        $statement->bindValue('id_user', $comment->getIdUser(), PDO::PARAM_INT);
        $statement->bindValue('id_gift', $comment->getIdGift(), PDO::PARAM_INT);
        $statement->bindValue('isPublic', $comment->getIsPublic(), PDO::PARAM_BOOL);

        $statement->bindValue('id', $comment->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Permet de supprimer un commentaire
     * @param Comment
     * @return void
     */
    public function delete(Comment $comment) {
        $statement = $this->connection->prepare('DELETE FROM comment WHERE id=:id');
        $statement->bindValue('id', $comment->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Permet de trouver tous les commentaires liés à un cadeau
     * @param int id du cadeau
     * @return array Comment
     */
    public function findByGift(int $id):array{

        $array = [];

        $statement = $this->connection->prepare('SELECT * FROM `comment` WHERE id_gift = :id ;
        ');

        $statement->bindValue('id', $id, PDO::PARAM_INT);


        $statement->execute();

        $results = $statement->fetchAll();
        if($results){
        foreach ($results as $line) {
            $array[] = $this->sqlToComment($line);
        }
        }
        return $array;
    }
}
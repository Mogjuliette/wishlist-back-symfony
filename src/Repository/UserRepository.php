<?php

namespace App\Repository;

use App\Entity\User;
use PDO;

/*Dans notre repository, on code les différentes méthodes correspondant au CRUD (CREATE, READ, UPDATE, DELETE) ainsi que des méthodes customs lorsque l'on a un besoin plus spécifique*/
class UserRepository{

    /**On instancie une connexion à la base de données avec PDO */
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    
    /**
     * permet de trouver tous les utilisateurs
     * @return array User
     */
    public function findAll(): array
    {
        /** @var User[] */
        $users = [];


        $statement = $this->connection->prepare('SELECT * FROM user');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $users[] = $this->sqlToUser($item);
        }
        return $users;
    }

    /**
     * Méthode permettant de trouver un utilisateur en fonction de son id
     * @param int $id
     * @return User|null
     */
    public function findById(int $id): User|null{
        $statement = $this->connection->prepare('SELECT * FROM user WHERE id = :id');
        $statement->bindParam(':id', $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();
        if($result === false){
            return null;
        }
        $user = $this->sqlToUser($result);
        return $user;
    }

    /**
     * Méthode pour factoriser la transformation d'une requête sql (tableau) en entités User
     * @param array
     * @return User
     */
    public function findByEmail(string $email): User|null{
        $statement = $this->connection->prepare('SELECT * FROM user WHERE email = :email');
        $statement->bindParam(':email', $email, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch();
        if($result === false){
            return null;
        }
        $user = $this->sqlToUser($result);
        return $user;
    }

    /**
     * Méthode pour factoriser la transformation d'une requête sql (tableau) en entités Gift
     * @param array
     * @return User
     */
    private function sqlToUser(array $item):User {
        return new User($item['username'], $item['email'], $item['password'], $item['role'], $item['id']);
    }

    /**
     * Pour enregistrer un nouveau utilisateur dans la base de données
     * @param User
     * @return void
     */
    public function persist(User $user): void{
        $statement = $this->connection->prepare('INSERT INTO user (username, email, password, role) VALUES (:username, :email, :password, "ROLE_USER")');
        $statement->bindValue('username', $user->getUsername(), PDO::PARAM_STR);
        $statement->bindValue('email', $user->getEmail(), PDO::PARAM_STR);
        $statement->bindValue('password', $user->getPassword(), PDO::PARAM_STR);
        $statement->execute();
        $user->setId($this->connection->lastInsertId());
    }

    /**
     * Pour mettre à jour un utilisateur dans la base de données
     * @param User
     * @return void
     */
    public function update(User $user) {
        $statement = $this->connection->prepare('UPDATE user SET password=:password email=:email WHERE id=:id');
        $statement->bindValue('password', $user->getPassword(), PDO::PARAM_STR);
        $statement->bindValue('email', $user->getEmail(), PDO::PARAM_STR);
        $statement->bindValue('id', $user->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Pour supprimer un utilisateur
     * @param User
     * @return void
     */
    public function delete(User $user) {
        $statement = $this->connection->prepare('DELETE FROM user WHERE id=:id');
        $statement->bindValue('id', $user->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Pour trouver tous les utilisateur d'un événement
     * @param int
     * @return array User
     */
    public function findByEvent(int $id):array{

        $array = [];

        $statement = $this->connection->prepare('SELECT * FROM `user` LEFT JOIN `userEvent` ue ON ue.id_user=`user`.id WHERE ue.id_event = :id ;
        ');

        $statement->bindValue('id', $id, PDO::PARAM_INT);


        $statement->execute();

        $results = $statement->fetchAll();
        if($results){
        foreach ($results as $line) {
            $array[] = $this->sqlToUser($line);
        }
        }
        return $array;
    }

}
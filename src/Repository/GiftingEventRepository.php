<?php

namespace App\Repository;

use App\Entity\GiftingEvent;
use PDO;

/*Dans notre repository, on code les différentes méthodes correspondant au CRUD (CREATE, READ, UPDATE, DELETE) ainsi que des méthodes customs lorsque l'on a un besoin plus spécifique*/
class GiftingEventRepository{

    /**On instancie une connexion à la base de données avec PDO */
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * permet de trouver tous les événements
     * @return array GiftingEvent
     */
    public function findall(): array
    {
        $giftingEvents = [];


        $statement = $this->connection->prepare('SELECT * FROM giftingEvent');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $giftingEvents[] = $this->sqlToGiftingEvent($item);
        }
        return $giftingEvents;
    
    }

    /**
     * Méthode permettant de trouver un événement en fonction de son id
     * @param int $id
     * @return GiftingEvent|null
     */
    public function findById(int $id): GiftingEvent|null{
        $statement = $this->connection->prepare('SELECT * FROM giftingEvent WHERE id = :id');
        $statement->bindParam(':id', $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();
        if($result === false){
            return null;
        }
        $user = $this->sqlToGiftingEvent($result);
        return $user;
    }

    /**
     * Méthode pour factoriser la transformation d'une requête sql (tableau) en entités GiftingEvent
     * @param array
     * @return GiftingEvent
     */
    private function sqlToGiftingEvent(array $item):GiftingEvent {
        return new GiftingEvent($item['eventName'], $item['id']);
    }

    /**
     * Pour enregistrer un nouvel événement dans la base de données
     * @param GiftingEvent
     * @return void
     */
    public function persist(GiftingEvent $giftingEvent): void{
        $statement = $this->connection->prepare('INSERT INTO giftingEvent (eventName) VALUES (:eventName)');
        $statement->bindValue('eventName', $giftingEvent->getEventName());
        $statement->execute();
        $giftingEvent->setId($this->connection->lastInsertId());
    }

    /**
     * Pour mettre à jour un événement dans la base de données
     * @param GiftingEvent
     * @return void
     */
    public function update(GiftingEvent $giftingEvent):void {
        $statement = $this->connection->prepare('UPDATE giftingEvent SET eventName =:eventName WHERE id=:id');
        $statement->bindValue('eventName', $giftingEvent->getEventName(), PDO::PARAM_STR);
        $statement->bindValue('id', $giftingEvent->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Pour supprimer un événement
     * @param GiftingEvent
     * @return void
     */
    public function delete(GiftingEvent $giftingEvent):void {
        $statement = $this->connection->prepare('DELETE FROM giftingEvent WHERE id=:id');
        $statement->bindValue('id', $giftingEvent->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Pour trouver tous les événements auquels participe un utilisateur
     * @param int
     * @return array GiftingEvent
     */
    public function findByUser(int $id):array{

        $array = [];

        $statement = $this->connection->prepare('SELECT * FROM `giftingEvent` LEFT JOIN `userEvent` ue ON ue.id_event=`giftingEvent`.id WHERE ue.id_user = :id ;
        ');

        $statement->bindValue('id', $id, PDO::PARAM_INT);


        $statement->execute();

        $results = $statement->fetchAll();
        if($results){
        foreach ($results as $line) {
            $array[] = $this->sqlToGiftingEvent($line);
        }
        }
        return $array;
    }

}
<?php
namespace App\Controller;

use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Repository\UserRepository;



class UserProvider implements UserProviderInterface {

public function __construct(private UserRepository $repo){}
/**
 * Méthode qui sera utilisée par Symfony lors d'une tentative de connexion pour
 * récupérer le User s'il existe. On y utilise notre repository pour chercher
 * le user dans la base de données
 */
public function loadUserByIdentifier(string $identifier):UserInterface {
    $user = $this->repo->findByEmail($identifier);
    if($user == null) {
        throw new UserNotFoundException();
    }
    return $user;
}
/**
 * Méthode utilisée pour mettre à jour un user stockée en session, dans le cas
 * d'une connexion en JWT, cette méthode n'est pas très importante, on lui fait
 * juste relancer la méthode load
 */
public function refreshUser(UserInterface $userInterface):UserInterface {
    return $this->loadUserByIdentifier($userInterface->getUserIdentifier());
}

/**
 * Méthode qui indique pour classe classe ce Provider devra s'appliquer. Ici ça
 * sera pour notre classe User. Cette méthode est surtout importante pour le cas
 * où on a plusieurs classes UserInterface différentes (ce qui arrive rarement)
 */ 
public function supportsClass(string $class):bool {
    return $class == User::class;
}
}
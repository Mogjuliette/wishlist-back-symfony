<?php

namespace App\Controller;

use App\Entity\GiftingEvent;
use App\Repository\GiftingEventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**Dans notre contrôleur, on crée les différentes méthodes avec les routes qui leur sont associées et seront appelées par le front via du HTTP */

/**On définit une route globale pour tout ce contrôleur avec l'annotation suivante : */
#[Route('/api/giftingEvent')]
class GiftingEventController extends AbstractController
{
    /*On crée une instance de notre repository correspondant*/
    private GiftingEventRepository $repo;

    public function __construct(GiftingEventRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Pour récupérer tous les événements
     */
    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

    /**
     * Pour récupérer tous les événements d'un utilisateur
     */
    #[Route('/user/{id}', methods: 'GET')]
    public function eventsById(int $id): JsonResponse
    {
        return $this->json($this->repo->findByUser($id));
    }

    /**
     * Pour récupérer un événement en fonction de son id
     */
    #[Route('/{id}', methods: 'GET')]
    public function one(int $id): JsonResponse
    {
        return $this->json($this->repo->findById($id));
    }

    /**
     * Pour enregistrer un nouvel événément
     */
    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        try { 
            $giftingEvent = $serializer->deserialize($request->getContent(), GiftingEvent::class, 'json');
         } catch (\Exception $e) {
            return $this->json('Invalid body', 400);
        }
        $errors = $validator->validate($giftingEvent);
        if ($errors->count() > 0) {
            return $this->json($errors, 400);
        } 

        $this->repo->persist($giftingEvent);
        return $this->json($giftingEvent, 201);
    }

    /**
     * Pour modifier un événement
     */
    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $giftingEvent = $this->repo->findById($id);
        if(!$giftingEvent){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), GiftingEvent::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
    }

    /**
     * Pour supprimer un événement
     */
    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {   
        $giftingEvent = $this->repo->findById($id);
        if(!$giftingEvent){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($giftingEvent);
        return $this->json('gifting event has been deleted', 202);
    }
}
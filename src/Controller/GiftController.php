<?php

namespace App\Controller;

use App\Entity\Gift;
use App\Repository\GiftRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**Dans notre contrôleur, on crée les différentes méthodes avec les routes qui leur sont associées et seront appelées par le front via du HTTP */

/**On définit une route globale pour tout ce contrôleur avec l'annotation suivante : */
#[Route('/api/gift')]
class GiftController extends AbstractController
{
    /*On crée une instance de notre repository correspondant*/
    private GiftRepository $repo;

    public function __construct(GiftRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Pour récupérer tous les cadeaux
     */
    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }


    /**
     * Pour récupérer tous les cadeaux d'un utilisateur pour un événément
     */
    #[Route('/user/{idUser}/event/{idEvent}', methods: 'GET')]
    public function giftByUserAndEvent(int $idUser, int $idEvent): JsonResponse
    {
        return $this->json($this->repo->findByUserAndEvent($idUser, $idEvent));
    }

    /**
     * Pour récupérer un cadeau avec son id
     */
    #[Route('/{id}', methods: 'GET')]
    public function one(int $id): JsonResponse
    {
        return $this->json($this->repo->findById($id));
    }

    /**
     * Pour enregistrer un nouveau cadeau 
     */
    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        try { 
            $gift = $serializer->deserialize($request->getContent(), Gift::class, 'json');
         } catch (\Exception $e) {
            return $this->json('Invalid body', 400);
        }
        $errors = $validator->validate($gift);
        if ($errors->count() > 0) {
            return $this->json($errors, 400);
        } 

        $this->repo->persist($gift);
        return $this->json($gift, 201);
    }

    /**
     * Pour modifier un cadeau
     */
    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $gift = $this->repo->findById($id);
        if(!$gift){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Gift::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
    }

    /**
     * Pour supprimer un cadeau
     */
    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {   
        $gift = $this->repo->findById($id);
        if(!$gift){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($gift);
        return $this->json('gift has been deleted', 202);
    }    
    
    /**
     * Pour passer modifier le cadeau de telle sorte à ce qu'il passe de "pas encore offert" à "quelqu'un va l'offrir"
     */
    #[Route('/{idGift}/offer/{idUser}', methods: 'PUT')]
    public function offer(int $idGift, int $idUser){
        $gift = $this->repo->findById($idGift);
        $gift->setIsOffered(true);
        $gift->setIdUserOfferingGift($idUser);
        $this->repo->update($gift);
        return $this->json($gift);
    }

    /**
     * Pour annuler le fait de vouloir offrir un certain cadeau
     */
    #[Route('/{idGift}/deleteOffer', methods: 'PUT')]
    public function deleteOffer(int $idGift){
        $gift = $this->repo->findById($idGift);
        $gift->setIsOffered(false);
        $gift->setIdUserOfferingGift(0);
        $this->repo->update($gift);
        return $this->json($gift);
    }
}
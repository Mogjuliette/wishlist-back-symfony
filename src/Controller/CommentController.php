<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**Dans notre contrôleur, on crée les différentes méthodes avec les routes qui leur sont associées et seront appelées par le front via du HTTP */

/**On définit une route globale pour tout ce contrôleur avec l'annotation suivante : */
#[Route('/api/comment')]
class CommentController extends AbstractController
{
    /*On crée une instance de notre repository correspondant*/
    private CommentRepository $repo;

    public function __construct(CommentRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Pour récupérer tous les commentaire
     */
    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

    /**
     * Pour récupérer un commentaire avec son id
     */
    #[Route('/{id}', methods: 'GET')]
    public function one(int $id): JsonResponse
    {
        return $this->json($this->repo->findById($id));
    }

    /**
     * Pour poster un commentaire
     */
    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        try { 
            $comment = $serializer->deserialize($request->getContent(), Comment::class, 'json');
         } catch (\Exception $e) {
            return $this->json('Invalid body', 400);
        }
        $errors = $validator->validate($comment);
        if ($errors->count() > 0) {
            return $this->json($errors, 400);
        } 

        $this->repo->persist($comment);
        return $this->json($comment, 201);
    }

    /**
     * Pour modifier un commentaire
     */
    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $comment = $this->repo->findById($id);
        if(!$comment){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Comment::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
    }

    /**
     * Pour supprimer un commentaire
     */
    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {   
        $giftingEvent = $this->repo->findById($id);
        if(!$giftingEvent){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($giftingEvent);
        return $this->json('comment has been deleted', 202);
    }

    /**
     * Pour trouver tous les commentaires liés à un même cadeau, avec l'id de ce cadeau
     */
    #[Route('/gift/{id}', methods: 'GET')]
    public function commentBygift(int $id): JsonResponse
    {
        return $this->json($this->repo->findByGift($id));
    }
}
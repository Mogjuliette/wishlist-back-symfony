<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**Dans notre contrôleur, on crée les différentes méthodes avec les routes qui leur sont associées et seront appelées par le front via du HTTP */

/**On définit une route globale pour tout ce contrôleur avec l'annotation suivante : */
#[Route('/api/user')]
class UserController extends AbstractController
{
    /*On crée une instance de notre repository correspondant*/
    private UserRepository $repo;

    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Pour récupérer tous les utilisateurs 
     */
    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

    /**
     * Pour récupérer tous les utilisateurs d"un événement
     */
    #[Route('/event/{id}', methods: 'GET')]
    public function usersByEvent(int $id): JsonResponse
    {
        return $this->json($this->repo->findByEvent($id));
    }

    /**
     * Pour récupérer un utilisateur en fonction de son id
     */
    #[Route('/{id}', methods: 'GET')]
    public function one(int $id): JsonResponse
    {
        return $this->json($this->repo->findById($id));
    }

    /**
     * Pour modifier un utilisateur
     */
    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $user = $this->repo->findById($id);
        if(!$user){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), User::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
    }

    /**
     * Pour supprimer un utilisateur
     */
    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {   
        $user = $this->repo->findById($id);
        if(!$user){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($user);
        return $this->json('user has been deleted', 202);
    }


}
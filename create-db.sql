-- Active: 1674203942125@@127.0.0.1@3306@wishlist
DROP DATABASE wishlist ;

CREATE DATABASE wishlist
    DEFAULT CHARACTER SET = 'utf8mb4';

USE wishlist; 

 CREATE TABLE 
    user (
        id INT PRIMARY KEY AUTO_INCREMENT,
        username VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL,
        role VARCHAR(255)
    ); 

 CREATE TABLE
    giftingEvent (
        id INT PRIMARY KEY AUTO_INCREMENT,
        eventName VARCHAR (255)
    ); 

CREATE TABLE
    userEvent (
        id_user INT,
        id_event INT,
        PRIMARY KEY (id_user,id_event),
        FOREIGN KEY (id_user) REFERENCES user(id),
        FOREIGN KEY (id_event) REFERENCES giftingEvent(id) 
    );

CREATE TABLE
    gift (
        id INT PRIMARY KEY AUTO_INCREMENT,
        id_userWantingGift int,
        id_event int,
        name VARCHAR(255),
        description VARCHAR(3000),
        image VARCHAR(3000),
        isOffered BOOLEAN,
        id_userOfferingGift int
    );

CREATE TABLE
    comment (
        id INT PRIMARY KEY AUTO_INCREMENT,
        text VARCHAR(500),
        id_user INT,
        id_gift INT,
        isPublic BOOLEAN
    );

INSERT INTO user (username, email, password, role) VALUES ("Juliette", "juliette@gmail.com", "$2y$13$6OiabH2vJiUCbXqxr2g/fulYkNtz.7zq809LYcxV8D0p6XZTpJS66", "ROLE_USER"),("Sophie", "sophie@gmail.com", "$2y$13$yQpm4YkN11MnHoGxnidkB.26YSN8fwjVn68OSF.F6AP.yK1zIVBwa", "ROLE_USER"),("Clément","clement@gmail.com","$2y$13$XvfU4Y6wZt/GKwT.wPSYXOOeS88poBGaLRsMTWU4lqLKwovyonrQ.", "ROLE_USER"),('Loulou',"loulou@gmail.com",'$2y$13$jzmBkkcLCu5aL33hMK4AV.iq7vkVgjkJLh8uGBzuQzyIjH7K6aEme', "ROLE_USER"),('Alice', "alice@gmail.com",'$2y$13$nT605bMCN97nM.Kxc9dmn.jZFtg8woLsQwx32ziHKgICtNmSiq8Pe', "ROLE_USER"),('Bob', "bob@gmail.com",'$2y$13$Rj6Aob1K2CtJY88AEoglFuBGZ1yp8aMH/TV/y02Hvc0MYEEtJXDy.', "ROLE_USER"),('Dani',"dani@gmail.com",'$2y$13$I05DUj3V1MTRwi6Se0ziCuayMkZh99I9jyCxkF4JdWcg/cmxBrxN6', "ROLE_USER"),('Elise',"elise@gmail.com",'$2y$13$EgUDA/c05F7EkuX7fBRVG.G8yWpD5UKS9evNeNNbwqryanp76drru', "ROLE_USER"),('Fatima',"fatima@gmail.com",'$2y$13$Qv.009h7q1rWrU.1fGPhC.DHFrSF5geJK2MBut9OozJYAiEaID15S', "ROLE_USER"),('Gaby',"gaby@gmail.com",'$2y$13$EXtnHCGKBTfiJPNgWXitBenj0ks0kBvUKQqQiIBPQXmZDcReiv48e', "ROLE_USER"),('Marika',"marika@gmail.com",'$2y$13$LQJsuZloTnBnGVZeBvbYRuRAKeBTYqvMQ/Hel2.lpSSG2lcvhpJPa', "ROLE_USER"),('Ariane',"ariane@gmail.com",'$2y$13$yhXe51z0By/KmsxP0XGY9u1CqTXZ9hYK7Tx/NDts9MnsuTL5NM7km', "ROLE_USER");
INSERT into giftingEvent (eventName) VALUES ("Noël 2023"),("Noël montagne"),("Anniversaire Clément et Alice"),("Anniv Loulou");

INSERT into userEvent (id_user,id_event) VALUES (1,1),(1,2),(1,4),(2,1),(2,4),(3,2),(3,3),(4,2),(4,4),(5,2),(5,3),(6,2),(6,3),(7,2),(7,4),(8,2),(8,3),(8,4),(9,2),(9,3),(10,2),(10,3),(11,1),(11,4),(12,1),(12,4) ;

INSERT into gift (id_userWantingGift, id_event ,name, description, image, isOffered, id_userOfferingGift) VALUES (1, 1, "écharpe", "Une belle écharpe en laine pour aller avec mon manteau gris", "https://images.unsplash.com/photo-1583170492868-dd617c96fdee?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80", TRUE, 2),(2, 2, "Belle BD", "Pour compléter ma collection", "https://images.unsplash.com/photo-1630348637620-3d0584aa148f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1887&q=80", TRUE, 3), (3, 2, "Vélo", "Pour la ville", "https://images.unsplash.com/photo-1485965120184-e220f721d03e?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80", FALSE, 0), (1, 1, "Roman A Court of Mist and Fury", "En anglais", "https://images.unsplash.com/photo-1614544048536-0d28caf77f41?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1887&q=80", FALSE, 0), (1, 2, "Surprise", "J'aime toujours les surprises !", "https://images.unsplash.com/photo-1513201099705-a9746e1e201f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1974&q=80", FALSE, 0), (2, 1, "Du bon café", "Soit en capsules Nespresso soit moulu", "", FALSE, 0), (2,1,'Set de tasses à café','Pour des expressos','https://plus.unsplash.com/premium_photo-1676651425508-047b3743dd94?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1887&q=80',1,1),(11,1,'Un catamaran','Un Nacra F-17 (même si je l''aurai jamais !)','https://images.unsplash.com/photo-1598871223446-a304e212c39c?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1854&q=80',0,0),(11,1,'Des livres de ma liste de livres','Comme sur la liste que je vous avais envoyée','https://images.unsplash.com/photo-1520467795206-62e33627e6ce?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2069&q=80',1,12),(11,1,'Veste bordeau','Longue, de demie-saison','https://images.unsplash.com/photo-1674926701542-5ea633780e2a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1887&q=80',1,2),(12,1,'Des bijoux','Plutôt bracelets ou boucles d oreilles','https://images.unsplash.com/photo-1573446238824-c28afa0cd312?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1854&q=80',1,1),(1,1,'Des surprises !','Je vous fais confiance !','https://images.unsplash.com/photo-1607469256872-48074e807b0a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80',1, 12),(12,1,'Livres de couture','Avec des patrons pour surjeteuse','https://images.unsplash.com/photo-1630930678172-63343537a00a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1962&q=80',1, 11),(12,1,'Participation financière aux travaux','Pour la mezzanine de notre appart','https://images.unsplash.com/photo-1527788263495-3518a5c1c42d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1816&q=80',1, 2),(3, 2, "Bonnet", "", "", FALSE, 0),(4, 2, "Du bon fromage !", "", "", FALSE, 0),(4, 2, "De bonnes bouteilles", "", "", FALSE, 0);


INSERT INTO comment (text, id_user, id_gift, isPublic) VALUES ("Je pense offrir l'écharpe à Juliette, qui veut participer avec moi ?", 2, 1, 0),("Plutôt de quelle couleur ?", 2, 1, 1)



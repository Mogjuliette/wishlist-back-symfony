<?php
namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/*Dans ce fichier, on regroupe les tests liés à l'entité Gift*/
class GiftTest extends WebTestCase
{

    /*Teste si on a bien un gift d'id 1 */
    public function testGetById() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/gift/1');
        
        $this->assertResponseIsSuccessful();
        
        $body = json_decode($client->getResponse()->getContent(), true);
      
        $this->assertIsInt($body['id']);
        $this->assertIsString($body['name']);
        $this->assertIsString($body['description']);

    }
    /*Teste si on a bien aucun gift d'id 100 */
    public function testGetByIdNotFound() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/gift/100');

        $this->assertResponseStatusCodeSame(404);
    }

    /*Teste si on récupère bien un total de 14 cadeaux sur la fonction getAll() */
    public function testGetAll() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/gift');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertCount(14, $body);
        $item = $body[0];
        $this->assertIsInt($item['id']);
        $this->assertIsString($item['name']);
        $this->assertIsString($item['description']);
        
        
    }
 
}